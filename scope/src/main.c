/**
  ******************************************************************************
  * @file    FreeRTOS/FreeRTOS_DelayUntil/Src/main.c
  * @author  MCD Application Team
  * @version V1.0.2
  * @date    18-November-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

#include "main.h"

static void GUI_Task(void const *argument);
static void Signal_Task(void const *argument);
static void FFT_Task(void const *argument);
static void TouchPanel_TimerCallback(TimerHandle_t pxTimer);
static void SystemClock_Config(void);
static float32_t LPDFT(float complex *Xw, uint32_t indexMax);
static void FindMax(float32_t *fftSorted);
static void Tune(void);

static AppGlobals_s appGlobals;
static int indexB[7] = {15, 24, 33, 42, 55, 72, 90};
int32_t maxVec[6] = {0, 0, 0, 0, 0, 0};
float32_t estim[6], freq;
uint32_t maxIndexSolo;
#define M_PI 3.14159265358979323846
int32_t sig_iterator = 0;
//static float estimated_freq;
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  //CPU_CACHE_Enable();
  HAL_Init();
  SystemClock_Config();

  xTaskCreate((TaskFunction_t)GUI_Task, "GUI_Task", 1024, NULL, 1, &appGlobals.guiTaskId);
  xTaskCreate((TaskFunction_t)Signal_Task, "Signal_Task", 1024, NULL, 1, &appGlobals.signalTaskId);
  xTaskCreate((TaskFunction_t)FFT_Task, "FFT_Task", 1024, NULL, 1, &appGlobals.fftTaskId);
  appGlobals.touchPanelTimer = xTimerCreate ("Timer", pdMS_TO_TICKS(100), pdTRUE, &appGlobals.touchPanelTimerId, TouchPanel_TimerCallback );
  appGlobals.gestureQueue = xQueueCreate(1, sizeof(MTOUCH_GestureData_s));

  vTaskStartScheduler();
  
  for(;;);
}

void GUI_Task(void const *arg) {

	BSP_SDRAM_Init();
	BSP_TS_Init(LCD_GetXSize(), LCD_GetYSize());
	xTimerStart(appGlobals.touchPanelTimer, 0);

	__HAL_RCC_CRC_CLK_ENABLE();
	WM_SetCreateFlags(WM_CF_MEMDEV | WM_CF_MEMDEV_ON_REDRAW);
	GUI_Init();
	GUI_Clear();

	appGlobals.fftGraph = GRAPH_CreateEx(0, LCD_GetYSize()/2, LCD_GetXSize(), LCD_GetYSize()/2, WM_HBKWIN, WM_CF_SHOW, 0, GUI_ID_GRAPH1);
	appGlobals.fftGraphScale = GRAPH_SCALE_Create(LCD_GetYSize()/2-12, GUI_TA_LEFT, GRAPH_SCALE_CF_HORIZONTAL, 50);
	GRAPH_SCALE_SetFactor(appGlobals.fftGraphScale, (3*DEFAULT_AUDIO_IN_FREQ/DMA_BUFFER_LENGTH));
	GRAPH_AttachScale(appGlobals.fftGraph, appGlobals.fftGraphScale);
	appGlobals.fftGraphData = GRAPH_DATA_YT_Create(GUI_BLUE,FFT_LENGTH, NULL, 0);
	GRAPH_DATA_YT_SetAlign(appGlobals.fftGraphData, GRAPH_ALIGN_LEFT);
	GRAPH_AttachData(appGlobals.fftGraph, appGlobals.fftGraphData);

	xTaskNotifyGive(appGlobals.signalTaskId);

	for (;;) {
		GUI_Delay(10);
	}
}

static void scaleAxisXFloat(float32_t* tab, uint32_t len, uint32_t scale) {
	uint32_t index;
	for (index = 0; index < len/scale; index++)
		arm_mean_f32(tab + (index * scale), scale, tab+index);
}

static void scaleAxisYFloat(float32_t* tab, uint32_t len) {
	float32_t min;
	float32_t max;
	uint32_t index;

	arm_max_f32(tab, len, &max, &index);
	arm_min_f32(tab, len, &min, &index);

	for (index = 0; index < len; index++)
		tab[index] = (tab[index]-min)*GRAPH_RANGE_Y/(max-min)+GRAPH_OFFSET_Y;
}

void Signal_Task(void const *arg) {

	ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

	BSP_AUDIO_IN_Init(INPUT_DEVICE_INPUT_LINE_1, DEFAULT_AUDIO_IN_VOLUME, I2S_AUDIOFREQ_16K);
	BSP_AUDIO_IN_Record((uint16_t*)appGlobals.dmaBuffer, DMA_BUFFER_LENGTH);

	uint32_t notificationValue;
	for (;;) {
		if (xTaskNotifyWait(0, UINT32_MAX, &notificationValue, portMAX_DELAY)) {
			if(notificationValue & TASK_EVENT_FFT_READY){
				Tune();
				GUI_Clear();
				GUI_DispStringAt("polyTUNER", 0, 0);

				for(int i = 0; i < 6; i++){
					GUI_DispDecAt(maxVec[i], 70*i, 30, 3);
					GUI_DispStringAt("", 70*i, 45);
					GUI_DispFloat(estim[i], 7);
				}
				GUI_Delay(100);
			}
		}
	 }
}

void FFT_Task(void const *arg) {

	arm_rfft_fast_instance_f32 fftInit;
	arm_rfft_fast_init_f32(&fftInit, SIGNAL_SAMPLES);

	int16_t displayOffsetX = 0;
	int16_t displayScaleX = 1;

	uint32_t notificationValue;
	uint16_t idx;
	for (;;) {

		if (xTaskNotifyWait(0, UINT32_MAX, &notificationValue, portMAX_DELAY)) {
			if(notificationValue & TASK_EVENT_DMA_HALF_DONE) {
				for(idx=0; idx<SIGNAL_SAMPLES; idx++)
					appGlobals.fftInOut[idx] = appGlobals.dmaBuffer[idx*2];
				arm_rfft_fast_f32(&fftInit, appGlobals.fftInOut, appGlobals.fftInOut, 0);
				arm_abs_f32(appGlobals.fftInOut, appGlobals.fftAbs, SIGNAL_SAMPLES);

				xTaskNotify(appGlobals.signalTaskId, TASK_EVENT_FFT_READY, eSetBits);
				scaleAxisXFloat(appGlobals.fftAbs, SIGNAL_SAMPLES, displayScaleX);
				scaleAxisYFloat(appGlobals.fftAbs, SIGNAL_SAMPLES/displayScaleX);
				for (idx = 0; idx < FFT_LENGTH; idx++) {
					appGlobals.fftDisplay[idx] = appGlobals.fftAbs[idx+displayOffsetX/displayScaleX];
				}

				GRAPH_DetachData(appGlobals.fftGraph, appGlobals.fftGraphData);
				GRAPH_DATA_YT_Delete(appGlobals.fftGraphData);
				appGlobals.fftGraphData = GRAPH_DATA_YT_Create(GUI_BLUE, FFT_LENGTH, appGlobals.fftDisplay, FFT_LENGTH);
				GRAPH_AttachData(appGlobals.fftGraph, appGlobals.fftGraphData);

			} if(notificationValue & TASK_EVENT_DMA_DONE) {
				for(idx=0; idx<SIGNAL_SAMPLES; idx++)
					appGlobals.fftInOut[idx] = appGlobals.dmaBuffer[DMA_BUFFER_LENGTH/2+idx*2];
				arm_rfft_fast_f32(&fftInit, appGlobals.fftInOut, appGlobals.fftInOut, 0);
				arm_abs_f32(appGlobals.fftInOut, appGlobals.fftAbs, SIGNAL_SAMPLES);

				xTaskNotify(appGlobals.signalTaskId, TASK_EVENT_FFT_READY, eSetBits);

				scaleAxisXFloat(appGlobals.fftAbs, SIGNAL_SAMPLES, displayScaleX);
				scaleAxisYFloat(appGlobals.fftAbs, SIGNAL_SAMPLES/displayScaleX);
				for (idx = 0; idx < FFT_LENGTH; idx++) {
					appGlobals.fftDisplay[idx] = appGlobals.fftAbs[idx+displayOffsetX/displayScaleX];
				}

				GRAPH_DetachData(appGlobals.fftGraph, appGlobals.fftGraphData);
				GRAPH_DATA_YT_Delete(appGlobals.fftGraphData);
				appGlobals.fftGraphData = GRAPH_DATA_YT_Create(GUI_BLUE, FFT_LENGTH, appGlobals.fftDisplay, FFT_LENGTH);
				GRAPH_AttachData(appGlobals.fftGraph, appGlobals.fftGraphData);

			}
		}
	 }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 216000000
  *            HCLK(Hz)                       = 216000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 432;  
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);

  SystemCoreClockUpdate();

}

static void FindMax(float32_t *fftSorted){
	int size = 0;
	float32_t maxVal[6], avgFFT, var;
	uint32_t maxIndex = 0;
	arm_mean_f32(fftSorted + 30, 70, &avgFFT);
	arm_var_f32(fftSorted + 30, 70, &var);

	float32_t sum = 0;
	for(int i = 0; i < 90; i++){
		sum += fftSorted[i];
	}
	sum /= 90;

	if(sum > 70){
		for(int i = 0; i < 6; i++){
			//copy fft in between indexes to buffer
			size = indexB[i+1] - indexB[i];

			arm_max_f32(fftSorted + indexB[i], size, maxVal + i, &maxIndex);
			maxIndex += indexB[i];
			//check if found maximum is really a local maximum
			if( (fftSorted[maxIndex-1] < fftSorted[maxIndex]) && ( fftSorted[maxIndex + 1] < fftSorted[maxIndex]) && (maxVal[i] > sum*1.1) ){
				maxVec[i] = maxIndex;
			}else{
				maxVec[i] = 0;
			}
		}
	}else {
		for(int i = 0; i < 6; i++)
			maxVec[i] = 0;
	}
}

static float32_t LPDFT(float complex *Xw, uint32_t indexMax){
	int32_t N = SIGNAL_SAMPLES;

	int32_t k[3] = {indexMax - 1, indexMax, indexMax + 1};
	float dw, wkm1, wk, wkp1, we;
	float complex r, R, lam;
	dw = 2*M_PI/N;
	wkm1 = (k[0])*dw;
	wk = (k[1])*dw;
	wkp1 = (k[2])*dw;

	r = ( -cexp( -I*wk) + cexp( -I*wkm1) )/( -cexp(-I*wkp1) + cexp(-I*wk) );
	R = ( Xw[k[0]]-Xw[k[1]] )/( Xw[k[1]]-Xw[k[2]] );

	lam= cexp(I*wk)*(r-R)/( r*cexp(-I*2*M_PI/N)-R*cexp(I*2*M_PI/N) );
	we = cimag(clogf(lam));

	 return we;
}

static void Tune(void){
//	FindMax();
	int32_t N = SIGNAL_SAMPLES/32;
	float32_t fftSorted[N], Om;
	float complex fftComplex[N];
	for(int idx=0; idx<N; idx++){
		fftSorted[idx] = 20*log10(sqrt(appGlobals.fftInOut[idx*2]*appGlobals.fftInOut[idx*2] + appGlobals.fftInOut[idx*2 + 1]*appGlobals.fftInOut[idx*2 + 1]));
		fftComplex[idx] = appGlobals.fftInOut[idx*2] + appGlobals.fftInOut[idx*2 + 1]*I;
	}

	FindMax(fftSorted);

	for(int i = 0; i < 6; i++){
		if(maxVec[i] != 0){
			Om = LPDFT(fftComplex, maxVec[i]);
			estim[i] = Om/(2*M_PI)*16000.0 - 0.013;
		}else
			estim[i] = 0;
	}
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif

void vApplicationTickHook( void )
{
	HAL_IncTick();
}

void BSP_AUDIO_IN_HalfTransfer_CallBack(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xTaskNotifyFromISR(appGlobals.fftTaskId, TASK_EVENT_DMA_HALF_DONE, eSetBits, &xHigherPriorityTaskWoken);
	xTaskNotifyFromISR(appGlobals.signalTaskId, TASK_EVENT_DMA_HALF_DONE, eSetBits, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void BSP_AUDIO_IN_TransferComplete_CallBack(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xTaskNotifyFromISR(appGlobals.fftTaskId, TASK_EVENT_DMA_DONE, eSetBits, &xHigherPriorityTaskWoken);
	xTaskNotifyFromISR(appGlobals.signalTaskId, TASK_EVENT_DMA_DONE, eSetBits, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void TouchPanel_TimerCallback(TimerHandle_t pxTimer) {

	TS_StateTypeDef tsState;
	MTOUCH_TouchData_s touchData;
	MTOUCH_GestureData_s gestureData;

	BSP_TS_GetState(&tsState);

	touchData.points = tsState.touchDetected;
	touchData.x[0] = tsState.touchX[0];
	touchData.x[1] = tsState.touchX[1];
	touchData.y[0] = tsState.touchY[0];
	touchData.y[1] = tsState.touchY[1];

	MTOUCH_AddTouchData(&touchData);
	MTOUCH_GetGesture(&gestureData);

	xQueueOverwrite(appGlobals.gestureQueue, &gestureData);
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
