# PolyTuner - Stroik polifoniczny

Czyli analizator widma częstotliowściowego, estymujący wartość częstotliwości o 
maksymalnej amplitudzie w zadanym paśmie.

## Metoda

Widmo sygnało o długości 8192 próbek, próbkowanego z częstotliwością 16 KHz
analizowane jest przez algorytm w poszukiwaniu maksimów w zadanych sześciu pasmach.

Każde z pasm odpowiada jednej ze strun gitary. Indeks znalezionego maksimum 
jest następnie przekazywany funkcji(LPDFT), która na podstawie tego maximum i dwóch 
sąsiadujących mu wartości estymuje częstotliwość sygnału z dokładnością +- 0.07 Hz.
Taki wynik zdecydowanie przeważa nad dokładnością, którą zapewnia zwykłe zczytywanie
widmo, w przypadku obecnej konfiguracji z rozdzielczością ok. 4 Hz.

## Interfejs GUI

Na wyświetlaczu prezentowane są:
* nazwa projektu,
* widmo częstotliwościowe ze skalą oraz
* indeks maksimum widma dla danej struny
* obliczona częstotliwość danej struny.